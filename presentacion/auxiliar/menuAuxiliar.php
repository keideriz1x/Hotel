<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <div class="container-fluid">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/auxiliar/sesionAuxiliar.php") ?>"><img src="img/logo.png" width="100px"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Habitaciones
          </a>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/habitaciones/consultarHabitacion.php") ?>">Consultar Habitacion</a></li>
            <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/habitaciones/consultarTipoDeHabitacion.php") ?>">Consultar Tipo De Habitacion</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Servicios
          </a>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/servicios/consultarServicio.php") ?>">Consultar Servicio</a></li>
          </ul>
        </li>
      </ul>
      <div class="d-flex">
      <div class="collapse navbar-collapse ml-auto" id="navbarNavDarkDropdown">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <?php echo $_SESSION["img"] == "" ? '<i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>' : '<img src="' . $_SESSION["img"] . '" class="menuPerfil my-auto">' ?>
              Auxiliar: <?php echo $_SESSION["nombre"] . " " . $_SESSION["apellido"] ?>
            </a>
            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
              <li><a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a></li>
            </ul>
          </li>
        </ul>
      </div>
      </div>
    </div>
  </div>
</nav>