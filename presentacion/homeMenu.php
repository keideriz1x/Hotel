<?php
$error = 0;
if (isset($_GET["error"])) {
    $error = $_GET["error"];
}
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary d-flex justify-content-around">
    <div class="col-6 d-flex justify-content-center">
        <div class="my-auto">
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" style="width: 100px;"></a>
        </div>
    </div>

    <div class="col-6 d-flex justify-content-center">
        <?php
        if (isset($_SESSION["id"]) && $_SESSION["rol"] == "USER") { ?>
            <div class="dropdown">
                <a class="btn btn btn-outline-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <?php
                    if ($_SESSION["img"] == "") {
                    ?>
                        <i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>
                    <?php
                    } else {
                    ?>
                        <img src="" class="menuPerfil my-auto">
                    <?php
                    }
                    ?>
                    <?php echo $_SESSION["nombre"] . " " . $_SESSION["apellido"] ?>
                </a>

                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <li> <a class="dropdown-item" href="<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/editarPerfil.php") ?>">Editar Perfil</a></li>
                    <li><a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a></li>
                </ul>
            </div>
            </li>
            </ul>
        <?php
        } else {
        ?>
            <a href="?" data-bs-toggle="modal" data-bs-target="#exampleModal" class="d-flex text-white text-decoration-none px-2">
                <i class="far fa-user fa-lg my-auto px-2"></i>
                <div class=" ml-2 my-auto d-none d-md-none d-lg-block text-nowrap">Mi cuenta</div>
            </a>
        <?php
        }
        ?>

        <hr class="bg-primary border-2 border-top border-white mx-1" style="border-left: 2px dotted white;height:30px;width:1px">
        <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/car.php") ?>" class=" d-flex text-white text-decoration-none px-2">
            <i class="fas fa-2x fa-clipboard-list bi bi-cart3 my-auto position-relative overflow-hidden" style="border-radius: 8px;z-index: 1;"></i>
            <span class="text-dark text-nowrap bg-secondary text-center position-relative" style="font-size: 10px;line-height: 20px;border-radius: 50%;min-width: 20px;height: 20px;right: 1em;top: 1.5em;z-index: 3;">0</span>
        </a>
    </div>
</nav>

<div class="modal fade pt-5" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex flex-row-reverse">
                <button type="button" class="btn-close float-right" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
                <h5 class="modal-title text-center" id="exampleModalLabel">Iniciar sesión</h5>
            </div>
            <div class="modal-body">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/autenticar.php") ?> method="post">
                    <div class="form-group py-2">
                        <input type="email" name="correo" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" class="form-control" placeholder="Correo" required="required">
                    </div>
                    <div class="form-group py-2">
                        <input type="password" name="clave" minlength="4" maxlength="15" class="form-control" placeholder="Clave" required="required">
                    </div>
                    <div class="form-group py-2 d-flex justify-content-center">
                        <button type="submit" class="btn btn-outline-primary">Iniciar sesion</button>
                    </div>
                    <p class="text-center">
                        <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/olvidoClave.php") ?>" class="text-primary">¿Olvido su contraseña?</a>
                    </p>
                    <p class="text-center">
                        <a>o</a>
                    </p>
                    <p class="text-center">
                        <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/crearCuenta.php") ?>" class="text-primary">Crear una cuenta</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if ($error == 1) { ?>
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Usuario y/o contraseña incorrecto',
        })
    </script>
<?php } else if ($error == 2) { ?>
    <<script>
        Swal.fire({
        icon: 'warning',
        text: 'Usuario deshabilitado',
        })
        </script>
    <?php } ?>