<?php

include "presentacion/homeMenu.php";

if (isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["correo"]) && isset($_POST["clave"]) && isset($_POST["cClave"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $cClave = $_POST["cClave"];

    if ($cClave == $clave) {
        $url = "https://udmyhotelproject.herokuapp.com/myhotel/usuario";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = json_encode(array(
            "nombre" => strtolower($nombre), "apellido" => $apellido,
            "correo" => strtolower($correo), "clave" => $clave,
            "rol" => "USER"
        ), JSON_FORCE_OBJECT);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        $data = json_decode($resp, true);

        curl_close($curl);
        
    }
}

?>

<div class="col-lg-4 container pt-5">
    <div class="card">
        <div class="card-header text-center bg-primary text-white rounded">
            <h3>Crear Cuenta</h3>
        </div>
        <div class="card-body">
            <form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/crearCuenta.php") ?> onsubmit="return validacion();" method="post">
                <div class="form-group pt-3">
                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
                </div>
                <div class="form-group pt-3">
                    <input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
                </div>
                <div class="form-group pt-3">
                    <input type="email" id="correo" name="correo" class="form-control" placeholder="Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
                </div>
                <div class="form-group pt-3">
                    <input type="password" id="clave" name="clave" class="form-control redo" placeholder="Contraseña" required="required" autocomplete="off" minlength="4" maxlength="15">
                </div>
                <div class="form-group pt-3">
                    <input type="password" id="cClave" name="cClave" class="form-control redo" placeholder="Confirmar Contraseña" required="required" autocomplete="off" minlength="4" maxlength="15">
                </div>
                <div class="form-group text-center pt-3">

                    <button type="submit" name="create_cuenta" class="btn btn-outline-primary">Crear cuenta</button>
                </div>
            </form>

        </div>
    </div>
    <?php
    if (isset($data["msg"]) && $data["msg"] == "Ok") { ?>
        <div class="alert alert-success mt-3" role="alert">
            Cuenta creada exitosamente...
        </div>
    <?php
    }
    ?>
    <?php
    if (isset($data["msg"]) && $data["msg"] == "error") { ?>
        <div class="alert alert-danger mt-3" role="alert">
            Este correo ya esta vinculado a una cuenta.
        </div>
    <?php
    }
    ?>
</div>

<script>
    $(function() {
        $(document).on('keyup', '#clave, #cClave', function() {
            var clave = $('#clave').val().trim();
            var cClave = $('#cClave').val().trim();
            if (!clave || !cClave || clave == '' || cClave == '') {
                $('#clave').removeClass('is-valid');
                $('#cClave').removeClass('is-valid');
            } else {
                if (clave !== cClave) {
                    $('#clave').removeClass('is-valid').addClass('is-invalid');
                    $('#cClave').removeClass('is-valid').addClass('is-invalid');
                } else {
                    $('#clave').removeClass('is-invalid').addClass('is-valid');
                    $('#cClave').removeClass('is-invalid').addClass('is-valid');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    function validacion() {
        //obteniendo el valor que se puso en campo text del formulario
        var clave = $("#clave").val();
        var cClave = $("#cClave").val();
        //la condición
        if ((clave != cClave) || (clave != cClave)) {
            Swal.fire({
                icon: 'error',
                title: 'Contraseñas no coinciden.',
            });
            return false;
        }
        return true;
    }
</script>