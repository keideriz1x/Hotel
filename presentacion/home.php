<?php
if (!isset($_SESSION["id"])) {
    include "presentacion/homeMenu.php";
}
?>
<div class="col-md-8 container">
    <?php
    include "presentacion/carousel.php";
    ?>
    <div class="col-xs-12 pt-2">
        <div class="input-group d-flex align-items-center">
            <div class="input-group-prepend px-2 my-auto">
                <h5 class="text-primary">My Hotel</h5>
            </div>
            <div class="form-group col">
                <hr class="bg-primary border-2 border-top border-primary">
            </div>
            <span class="input-group-prepend">
                <div class="holder">
                </div>
            </span>
        </div>
    </div>
    <?php
    include "presentacion/vistaProductos.php";
    ?>

</div>