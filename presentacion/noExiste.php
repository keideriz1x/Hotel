<div class='col-lg-4 container pt-5'>
    <div class='row mt-4'>

        <div class='col-lg-12'>
            <div class='card'>
                <div class='card-body py-5'>
                    <h2 class='text-center'>La pagina a la que quiere ingresar no existe o no tiene permisos</h2>
                </div>
            </div>
        </div>
    </div>
</div>