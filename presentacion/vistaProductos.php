
<link rel="stylesheet" type="text/css" href="css/jPages.css">
<script src="https://cdn.shopify.com/s/files/1/0771/2161/t/3/assets/jPages.min.js?10688064064350896456"></script>

<div id="itemContainer" class="row">
    <?php
    foreach ($productos as $productoActual) { ?>

        <div class="col-md-3 col-sm-6 pb-5">
            <div class="product-grid" id="pr">
                <div class="product-image">
                    <a href="<?php echo "index.php?pid=" . encrypt("presentacion/producto.php",$key). "&p=" . base64_encode($productoActual->getIdProducto()) ?>" class="image">

                        <?php
                        $foto = new Foto("", "", $productoActual->getIdProducto());
                        $fotos = $foto->mostrarTodos();
                        $contador = 0;
                        if (!empty($fotos) and count($fotos) > 0) {
                            foreach ($fotos as $fotoActual) { ?>

                                <?php if ($contador == 0) { ?>
                                    <img class="pic-1" src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>" />
                                <?php $contador = 1;
                                }
                                if ($contador == 1) { ?>
                                    <img class="pic-2" src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>" />
                                <?php   
                            } 
                            ?>

                        <?php
                            }
                        }
                        ?>

                    </a>
                    <ul class="product-links">
                        <li><a href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto.php"). "&p=" . base64_encode($productoActual->getIdProducto()) ?>"> <i class="fa fa-eye"></i></a></li>
                        <li><a class="carritoP" data-toggle="modal" data-target="#id<?php echo $productoActual->getIdProducto() ?>"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto.php"). "&p=" . base64_encode($productoActual->getIdProducto()) ?>"><?php echo $productoActual->getNombre() ?></a></h3>
                    <div class="price">$ <?php echo number_format($productoActual->getPrecio(), 0, ',', '.') ?> COP</div>
                </div>
            </div>
        </div>

        <div class="contenedor modal fade pt-5" id="id<?php echo $productoActual->getIdProducto() ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-break"><?php echo $productoActual->getNombre() ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">

                                <?php
                                $foto = new Foto("", "", $productoActual->getIdProducto());
                                $fotos = $foto->mostrarTodos();
                                $contador = 0;
                                if (!empty($fotos) and count($fotos) > 0) {

                                    foreach ($fotos as $fotoActual) { ?>

                                        <?php if ($contador == 0) { ?>
                                            <img src="data:image/jpeg;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>" class="imgcarritoinicio mx-auto d-block">
                                        <?php $contador = 1;
                                        } else { ?>

                                        <?php   } ?>

                                <?php
                                    }
                                }
                                ?>

                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="mb-5"></div>
                                <p class="mb-10">En Carrito</p>
                                <div class="d-flex">
                                    <a href="#" data-dismiss="modal" class="btn btn-success mr-2 carrito">Continuar</a>
                                    <a href="<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/car.php") ?>" class="btn btn-danger carrito ">Ir a Carrito</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
    }
    ?>
</div>


<div class="d-flex justify-content-between">
    <div class="holder">
    </div>
    <div>
        <form>
            <select class="form-control">
                <option>12</option>
                <option>24</option>
                <option>48</option>
            </select>
        </form>
    </div>
</div>

<script>
    $(function() {
        $("div.holder").jPages({
            containerID: "itemContainer",
            perPage: 12,
            previous: "Anterior",
            next: "Siguiente",
        });
        $("select").change(function() {

            var newPerPage = parseInt($(this).val());
            /* destroy jPages and initiate plugin again */
            $("div.holder").jPages("destroy").jPages({
                containerID: "itemContainer",
                perPage: newPerPage,
                previous: "Anterior",
                next: "Siguiente",
            });
        });
    });
</script>