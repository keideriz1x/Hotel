<?php

include "presentacion/homeMenu.php";

if (isset($_POST["correo"])) {
    $correo = $_POST["correo"];

    $url = "https://udmyhotelproject.herokuapp.com/myhotel/auth/olvido";

    $data = array("correo" => strtolower($correo));
    $data_json = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_json)));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    $data = json_decode($response, true);

    if (isset($data["message"])) {
        $message = $data["message"];
    }
    if (isset($data["description"])) {
        $description = $data["description"];
    }

    curl_close($ch);
}

?>

<div class="col-lg-5 container pt-5">
    <div class="card">
        <div class="card-header text-center text-white bg-primary rounded">
            <h3>Recupera tu cuenta</h3>
        </div>
        <div class="card-body text-center">
            <form id="clave" action=<?php echo "index.php?pid=" . base64_encode("presentacion/olvidoClave.php") ?> method="post">
                <div class="pb-2">Ingresa tu correo electrónico para enviar codigo de cambio de contraseña</div>
                <div class="form-group">
                    <input type="email" name="correo" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" id="correo" class="form-control" placeholder="Correo electronico" required="required" autocomplete="off">
                </div>
                <div class="form-group text-center pt-3">
                    <button type="submit" id="enviar" class="btn btn-outline-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
    <?php
    if (isset($message)) { ?>
        <div class="alert alert-success mt-3" role="alert">
            <?php echo $message; ?>
        </div>
    <?php
    }
    ?>
    <?php
    if (isset($description)) { ?>
        <div class="alert alert-danger mt-3" role="alert">
            Este correo no existe.
        </div>
    <?php
    }
    ?>
</div>