<?php

$correo = $_POST["correo"];
$clave = $_POST["clave"];

$url = "https://udmyhotelproject.herokuapp.com/myhotel/auth/login";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
   "Content-Type: application/json",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$data = json_encode(array("correo"=>strtolower($correo),"clave"=>$clave), JSON_FORCE_OBJECT);

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);

$data=json_decode($resp,true);

if(isset($data["cliente"])){
    $cliente=$data["cliente"];

    $_SESSION["id"] = $cliente["uid"];
    $_SESSION["rol"] = $cliente["rol"];
    $_SESSION["nombre"] = $cliente["nombre"];
    $_SESSION["apellido"] = $cliente["apellido"];
    $_SESSION["img"] = $cliente["img"];
    $_SESSION["correo"] = $cliente["correo"];
    $_SESSION["x-token"] = $data["token"];

    if ($cliente["rol"]=="ADMIN") {
       
        header("Location: index.php?pid=" . base64_encode("presentacion/administrador/sesionAdministrador.php"));
    }elseif ($cliente["rol"]=="AUX") {
       
        header("Location: index.php?pid=" . base64_encode("presentacion/domiciliario/sesionDomiciliario.php"));
    }elseif ($cliente["rol"]=="USER") {
        
        header("Location: index.php?pid=" . base64_encode("presentacion/home.php"));
    }

}

if (isset($data["msg"]) && $data["msg"]=="error crendeciales") {
    header("Location: index.php?error=1");
}

?>
