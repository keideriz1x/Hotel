<?php
$clientes = json_decode(file_get_contents('https://udmyhotelproject.herokuapp.com/myhotel/usuario/?limite=100'), true);
$usuario = $clientes["usuarios"];

$a = 0;
for ($i = 0; $i < count($usuario); $i++) {

  if ($usuario[$i]["rol"] == "USER") {

    $lista_clientes[$a]["uid"] = $usuario[$i]["uid"];
    $lista_clientes[$a]["identificacion"] = $usuario[$i]["identificacion"];
    $lista_clientes[$a]["nombre"] = $usuario[$i]["nombre"];
    $lista_clientes[$a]["apellido"] = $usuario[$i]["apellido"];
    $lista_clientes[$a]["correo"] = $usuario[$i]["correo"];
    $lista_clientes[$a]["telefono"] = $usuario[$i]["telefono"];
    $lista_clientes[$a]["estado"] = $usuario[$i]["estado"];

    $a++;
  }
}

?>

<div class="container pb-1">
  <div class="row mt-4">
    <div class="col-lg-12">
      <div class="card text-white">
        <div class="card-header bg-primary text-center rounded">
          <h3>Consultar Cliente</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="table table-striped table-bordered" style="width:100%">
              <thead class="table-dark">
              <th scope="col"> Nombre</th>
              <th scope="col"> Apellido</th>
                <th scope="col"> Correo</th>
                <th scope="col"> Telefono</th>
                <th scope="col"> Servicios</th>
              </thead>
              <tbody>
                <?php

                foreach ($lista_clientes as $clienteActual) {
                  echo "<tr>";
                  echo "<td>" . $clienteActual["nombre"] . "</td>";
                  echo "<td>" . $clienteActual["apellido"] . "</td>";
                  echo "<td>" . $clienteActual["correo"] . "</td>";
                  echo "<td>" . $clienteActual["telefono"] . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $clienteActual["uid"] . "'><button class='btn btn-sm btn-outline-primary' id='cl" . $clienteActual["uid"] . "'>" . (($clienteActual["estado"] == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($clienteActual["estado"] == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.addEventListener("resize", mostrar);
    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    new $.fn.dataTable.FixedHeader(table);
  }
</script>