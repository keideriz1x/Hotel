<?php

if (isset($_POST["create_categoria"])) {
	$c = new Categoria("", $_POST["nombre"], "");
	if ($c->verificar()) {
		$categoria = new Categoria("", $_POST["nombre"], 1);
		$categoria->crear();
		$log = new Log("", "Crear categoria", "Categoria: " . $_POST["nombre"], date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
		$log->crear();
		echo "<script>
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Categoria creada con exito.',
			showConfirmButton: false,
			timer: 1500
		})
		</script>";
	} else {
		echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Categoria ya existe.',
			})
		</script>";
	}
}

?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Categoria</h3>
				</div>
				<div class="card-body">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/categoria/crearCategoria.php") ?> method="post">
						<div class="form-group">
							<input id="nombre" maxlength="15" minlength="3" type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<button type="submit" name="create_categoria" class="btn btn-danger btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>