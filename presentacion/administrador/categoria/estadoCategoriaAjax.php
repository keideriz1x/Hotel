<?php
if (isset($_GET["idCategoria"]) && isset($_GET["estado"])) {

$categoria = new Categoria($_GET["idCategoria"], "", $_GET["estado"]);
$categoria -> editarEstado();

$consultar= new Categoria($_GET["idCategoria"], "", "");
$consultar->consultar();

$log = new Log("", "Editar estado de Categoria",$consultar->getNombre()." cambiado a: ". (($_GET["estado"]==1) ? "Activo" : "Restringido") , date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
$log->crear();

echo (($_GET["estado"]==1) ? "<div id='icono" . $_GET["idCategoria"] . "' class='text-light'>Activo</div>" : "<div id='icono" . $_GET["idCategoria"] . "' class='text-light'>Restringido</div>");
}
?>
