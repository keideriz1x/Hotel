
<?php
$idCategoria = null;
if (isset($_GET["id"])) {
    $idCategoria = decrypt($_GET["id"], $key);
}

$editado = false;
if(isset($_POST["editarCategoria"])){
    $categoria = new Categoria($idCategoria, $_POST["nombre"], 1);
    $categoria -> editar();
    $editado = true;
}else{
    $categoria = new Categoria($idCategoria);
    $categoria -> consultar();
}
?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Editar Categoria</h3>
				</div>
				<div class="card-body">
					<form id="editar" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/categoria/editarCategoria.php"). "&id=" . base64_encode($idCategoria) ?> method="post">
						<div class="form-group">
							<input id="nombre" maxlength="15" minlength="3" type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<button type="submit" name="editarCategoria" class="btn btn-danger btn-block">Editar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if ($editado) { ?>
	<script>
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Categoria editada con exito.',
			showConfirmButton: false,
			timer: 1500
		})
	</script>
<?php } ?>