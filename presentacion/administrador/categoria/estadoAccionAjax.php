<?php

if (isset($_GET["idCategoria"]) && isset($_GET["estado"])) {

	$idCategoria = $_GET["idCategoria"];
	$estado = $_GET["estado"];

	echo "<div id='accion" . $idCategoria . "'><a href='index.php?pid= " . base64_encode("presentacion/administrador/categoria/editarCategoria.php") . "&id=" . base64_encode($idCategoria) . "' class ='btn btn-sm'><i class='fas fa-edit text-light' data-bs-toggle='tooltip' title='Editar'></i></a><button class='btn btn-sm' id='estado" . $idCategoria . "' href='#' >" . (($estado == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($estado == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
}
?>
<?php
if (isset($_GET["idCategoria"]) && isset($_GET["estado"])) { ?>
	<script>
		$(document).ready(function() {
			$("#estado<?php echo $idCategoria ?>").click(function(e) {

				e.preventDefault();

				Swal.fire({
					title: '¿Seguro?',
					text: "¿Desea cambiar el estado de esta categoria?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, hacer cambio',
					cancelButtonText: "Cancelar"
				}).then((result) => {
					if (result.isConfirmed) {


						$('[data-toggle="tooltip"]').tooltip('hide');

						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/categoria/estadoCategoriaAjax.php") ?>&idCategoria=<?php echo $idCategoria ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#icono<?php echo $idCategoria ?>").load(url);
						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/categoria/estadoAccionAjax.php") ?>&idCategoria=<?php echo $idCategoria ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#accion<?php echo $idCategoria ?>").load(url);

					}
				})

			});
		});
	</script>
<?php
}
?>