<?php
$categoria = new Categoria();
$categorias = $categoria->consultarTodos();
?>

<div class="container pb-1">
  <div class="row mt-4 justify-content-center d-flex">
    <div class="col-lg-12">
      <div class="card text-white pt-4">
        <div class="cardAdmin card-header text-center rounded">
          <h3>Consultar Categoria</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="display" style="width:100%;color: black;">
              <thead style="color: white;">
                <th> Nombre</th>
                <th> Estado</th>
                <th> Servicios</th>
              </thead>
              <tbody>

                <?php

                foreach ($categorias as $categoriaActual) {
                  echo "<tr>";
                  echo "<td>" . $categoriaActual->getNombre() . "</td>";
                  echo "<td>" . (($categoriaActual->getEstado() == 1) ? "<div id='icono" . $categoriaActual->getIdCategoria() . "' class='text-light'>Activo</div>" : (($categoriaActual->getEstado() == 0) ? "<div id='icono" . $categoriaActual->getIdCategoria() . "' class='text-light'>Restringido</div>" : "Inactivo")) . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $categoriaActual->getIdCategoria() . "'><a href='index.php?pid= " . encrypt("presentacion/administrador/categoria/editarCategoria.php", $key) . "&id=" . encrypt($categoriaActual->getIdCategoria(), $key) . "' class ='btn btn-sm'><i class='fas fa-edit text-light' data-bs-toggle='tooltip' title='Editar'></i></a><button class='btn btn-sm' id='estado" . $categoriaActual->getIdCategoria() . "' href='#' >" . (($categoriaActual->getEstado() == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($categoriaActual->getEstado() == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
  foreach ($categorias as $categoriaActual) { ?>
    <script>
      $(document).ready(function() {
        $("#estado<?php echo $categoriaActual->getIdCategoria() ?>").click(function(e) {

          e.preventDefault();

          Swal.fire({
            title: '¿Seguro?',
            text: "¿Desea cambiar el estado de esta categoria?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, hacer cambio',
            cancelButtonText: "Cancelar"
          }).then((result) => {
            if (result.isConfirmed) {

              $('[data-toggle="tooltip"]').tooltip('hide');
              var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/categoria/estadoCategoriaAjax.php") ?>&idCategoria=<?php echo $categoriaActual->getIdCategoria() ?>&estado=<?php echo (($categoriaActual->getEstado() == 1) ? "0" : "1") ?>";
              $("#icono<?php echo $categoriaActual->getIdCategoria() ?>").load(url);
              var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/categoria/estadoAccionAjax.php") ?>&idCategoria=<?php echo $categoriaActual->getIdCategoria() ?>&estado=<?php echo (($categoriaActual->getEstado() == 1) ? "0" : "1") ?>";
              $("#accion<?php echo $categoriaActual->getIdCategoria() ?>").load(url);

            }
          })

        });
      });
    </script>
  <?php
  }
  ?>

  <script>
    $(document).ready(function() {

      mostrar();

    });

    function mostrar() {
      var table = $('#example-table').DataTable({
        "sort": true,
        "destroy": true,
        "lengthMenu": [
          [5, 10, 25, 50, -1],
          [5, 10, 25, 50, "All"]
        ],
        "columnDefs": [{
          "targets": 2,
          "orderable": false
        }, ],
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
      });
      new $.fn.dataTable.FixedHeader(table);

    }
  </script>