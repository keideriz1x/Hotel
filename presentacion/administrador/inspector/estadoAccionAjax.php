<?php

if (isset($_GET["idInspector"]) && isset($_GET["estado"])) {

	$idInspector = $_GET["idInspector"];
	$estado = $_GET["estado"];

	echo "<div id='accion" . $idInspector . "'><button class='btn btn-sm' id='ins" . $idInspector . "'>" . (($estado == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($estado == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
}
?>
<?php
if (isset($_GET["idInspector"]) && isset($_GET["estado"])) { ?>
	<script>
		$(document).ready(function() {
			$("#ins<?php echo $idInspector ?>").click(function(e) {

				e.preventDefault();

				Swal.fire({
					title: '¿Seguro?',
					text: "¿Desea cambiar el estado de este inspector?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, hacer cambio',
					cancelButtonText: "Cancelar"
				}).then((result) => {
					if (result.isConfirmed) {

						$('[data-toggle="tooltip"]').tooltip('hide');

						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/inspector/estadoInspectorAjax.php") ?>&idInspector=<?php echo $idInspector ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#icono<?php echo $idInspector ?>").load(url);
						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/inspector/estadoAccionAjax.php") ?>&idInspector=<?php echo $idInspector ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#accion<?php echo $idInspector ?>").load(url);

					}
				})

			});
		});
	</script>
<?php
}
?>