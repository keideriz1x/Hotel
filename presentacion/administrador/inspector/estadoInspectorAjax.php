<?php
if (isset($_GET["idInspector"]) && isset($_GET["estado"])) {

    $inspector = new Inspector($_GET["idInspector"], "", "", "", "", "", $_GET["estado"]);
    $inspector->editarEstado();

    $consultar = new Inspector($_GET["idInspector"], "", "");
    $consultar->consultar();

    $log = new Log("", "Editar estado de Inspector", "id: " . $consultar->getIdInspector() . " nombre: " . $consultar->getNombre() . " " . $consultar->getApellido() . " cambiado a: " . (($_GET["estado"] == 1) ? "Activo" : "Restringido"), date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
    $log->crear();

    echo (($_GET["estado"] == 1) ? "<div id='icono" . $_GET["idInspector"] . "' class='text-light'>Activo</div>" : "<div id='icono" . $_GET["idInspector"] . "' class='text-light'>Restringido</div>");
}
