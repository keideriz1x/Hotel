<?php
$inspector = new Inspector();
$inspectores = $inspector->consultarTodos();
?>

<div class="container pb-1">
  <div class="row mt-4 justify-content-center d-flex">
    <div class="col-lg-12">
      <div class="card text-white pt-4">
        <div class="cardAdmin card-header text-center rounded">
          <h3>Consultar Domiciliario</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="display" style="width:100%;color: black;">
              <thead style="color: white;">
                <th> Nombre</th>
                <th> Correo</th>
                <th> Estado</th>
                <th> Servicios</th>
              </thead>
              <tbody>

                <?php

                foreach ($inspectores as $inspectorActual) {
                  echo "<tr>";
                  echo "<td>" . $inspectorActual->getNombre() . " " . $inspectorActual->getApellido() . "</td>";
                  echo "<td>" . $inspectorActual->getCorreo() . "</td>";
                  echo "<td>" . (($inspectorActual->getEstado() == 1) ? "<div id='icono" . $inspectorActual->getIdInspector() . "' class='text-light'>Activo</div>" : (($inspectorActual->getEstado() == 0) ? "<div id='icono" . $inspectorActual->getIdInspector() . "' class='text-light'>Restringido</div>" : "Inactivo")) . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $inspectorActual->getIdInspector() . "'><button class='btn btn-sm' id='ins" . $inspectorActual->getIdInspector() . "'>" . (($inspectorActual->getEstado() == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($inspectorActual->getEstado() == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
foreach ($inspectores as $inspectorActual) { ?>
  <script>
    $(document).ready(function() {
      $("#ins<?php echo $inspectorActual->getIdInspector() ?>").click(function(e) {

        e.preventDefault();

        Swal.fire({
          title: '¿Seguro?',
          text: "¿Desea cambiar el estado de este inspector?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, hacer cambio',
          cancelButtonText: "Cancelar"
        }).then((result) => {
          if (result.isConfirmed) {

            $('[data-toggle="tooltip"]').tooltip('hide');
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/inspector/estadoInspectorAjax.php") ?>&idInspector=<?php echo $inspectorActual->getIdInspector() ?>&estado=<?php echo (($inspectorActual->getEstado() == 1) ? "0" : "1") ?>";
            $("#icono<?php echo $inspectorActual->getIdInspector() ?>").load(url);
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/inspector/estadoAccionAjax.php") ?>&idInspector=<?php echo $inspectorActual->getIdInspector() ?>&estado=<?php echo (($inspectorActual->getEstado() == 1) ? "0" : "1") ?>";
            $("#accion<?php echo $inspectorActual->getIdInspector() ?>").load(url);

          }
        })

      });
    });
  </script>
<?php
}
?>

<script>
  $(document).ready(function() {

    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "sort": true,
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "columnDefs": [{
        "targets": 3,
        "orderable": false
      }, ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      },
    });
    new $.fn.dataTable.FixedHeader(table);

  }
</script>