<?php
$habitacion = json_decode(file_get_contents('https://udmyhotelproject.herokuapp.com/myhotel/habitacion/?limite=100'), true);
$habitaciones = $habitacion["habitacion"];


$a = 0;
for ($i = 0; $i < count($habitaciones); $i++) {

    $prueba=json_encode($habitaciones[$i],JSON_FORCE_OBJECT);
    $tipohabitacion=json_decode($prueba,true);
    $tipo=json_encode($tipohabitacion["tipo_habitacion"],JSON_FORCE_OBJECT);
    $final=json_decode($tipo,true);

    $lista_habitaciones[$i]["numero"] = $habitaciones[$i]["numero"];
    $lista_habitaciones[$i]["id"] = $final["_id"];
    $lista_habitaciones[$i]["categoria"] = $final["categoria"];
    $lista_habitaciones[$i]["estado"] = $habitaciones[$i]["ocupado"];
    $lista_habitaciones[$i]["uid"] = $habitaciones[$i]["uid"];


}

?>

<div class="container pb-1">
  <div class="row mt-4">
    <div class="col-lg-12">
      <div class="card text-white">
        <div class="card-header bg-primary text-center rounded">
          <h3>Consultar Habitaciones</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="table table-striped table-bordered" style="width:100%">
              <thead class="table-dark">
              <th scope="col"> Numero</th>
              <th scope="col"> Categoria</th>
                <th scope="col"> Servicios</th>
              </thead>
              <tbody>
                <?php

                foreach ($lista_habitaciones as $habitacionActual) {
                  echo "<tr>";
                  echo "<td>" . $habitacionActual["numero"] . "</td>";
                  echo "<td>" . $habitacionActual["categoria"] . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $habitacionActual["uid"] . "'><button class='btn btn-sm btn-outline-primary' id='cl" . $habitacionActual["uid"] . "'>" . (($habitacionActual["estado"] == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($habitacionActual["estado"] == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.addEventListener("resize", mostrar);
    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    new $.fn.dataTable.FixedHeader(table);
  }
</script>