<?php
$tiposHabitacion = json_decode(file_get_contents('https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100'), true);
$tipo = $tiposHabitacion["habitacion"];

for ($i = 0; $i < count($tipo); $i++) {

    $lista_tiposHabitacion[$i]["uid"] = $tipo[$i]["uid"];
    $lista_tiposHabitacion[$i]["categoria"] = $tipo[$i]["categoria"];
    $lista_tiposHabitacion[$i]["camas"] = $tipo[$i]["camas"];
    $lista_tiposHabitacion[$i]["terraza"] = $tipo[$i]["terraza"];
    $lista_tiposHabitacion[$i]["precio"] = $tipo[$i]["precio"];
    $lista_tiposHabitacion[$i]["telefono"] = $tipo[$i]["precio"];
    $lista_tiposHabitacion[$i]["img"] = $tipo[$i]["img"];
    $lista_tiposHabitacion[$i]["estado"] = $tipo[$i]["estado"];
  
}

?>

<div class="container pb-1">
  <div class="row mt-4">
    <div class="col-lg-12">
      <div class="card text-white">
        <div class="card-header bg-primary text-center rounded">
          <h3>Consultar Tipo de Habitacion</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="table table-striped table-bordered" style="width:100%">
              <thead class="table-dark">
              <th scope="col"> Categoria</th>
              <th scope="col"> Camas</th>
                <th scope="col"> Terraza</th>
                <th scope="col"> Precio</th>
                <th scope="col"> Servicios</th>
              </thead>
              <tbody>
                <?php

                foreach ($lista_tiposHabitacion as $habitacionActual) {
                  echo "<tr>";
                  echo "<td>" . $habitacionActual["categoria"] . "</td>";
                  echo "<td>" . $habitacionActual["camas"] . "</td>";
                  echo "<td>";
                  echo "<div><span>" . (($habitacionActual["terraza"] == 1) ? "Si" : (($habitacionActual["terraza"] == 0) ? "No" : "")) . "</span></div>";
                  echo "</td>";
                  echo "<td>" . $habitacionActual["precio"] . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $habitacionActual["uid"] . "'><button class='btn btn-sm btn-outline-primary' id='cl" . $habitacionActual["uid"] . "'>" . (($habitacionActual["estado"] == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($habitacionActual["estado"] == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.addEventListener("resize", mostrar);
    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    new $.fn.dataTable.FixedHeader(table);
  }
</script>