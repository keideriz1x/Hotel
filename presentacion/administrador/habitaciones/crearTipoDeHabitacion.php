<?php

$url = "https://udmyhotelproject.herokuapp.com/myhotel/tipoH/";

if (isset($_POST["categoria"]) && isset($_POST["camas"]) && isset($_POST["precio"]) && isset($_POST["terraza"])) {

	$categoria = $_POST["categoria"];
	$camas = $_POST["camas"];
	$precio = $_POST["precio"];
	$terraza = (bool)$_POST["terraza"];
	$token = $_SESSION["x-token"];

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$headers = array(
		"x-token: $token",
		"Content-Type: application/json",
	);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	$data = json_encode(array("categoria" => strtolower($categoria), "camas" => $camas, "precio" => $precio, "terraza" => $terraza), JSON_FORCE_OBJECT);

	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$resp = curl_exec($curl);
	curl_close($curl);
	var_dump($resp);

	$data = json_decode($resp, true);
} elseif (isset($_POST["categoria"]) && isset($_POST["camas"]) && isset($_POST["precio"])) {

	$categoria = $_POST["categoria"];
	$camas = $_POST["camas"];
	$precio = $_POST["precio"];
	$token = $_SESSION["x-token"];

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$headers = array(
		"x-token: $token",
		"Content-Type: application/json",
	);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	$data = json_encode(array("categoria" => strtolower($categoria), "camas" => $camas, "precio" => $precio), JSON_FORCE_OBJECT);

	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$resp = curl_exec($curl);
	curl_close($curl);
	var_dump($resp);

	$data = json_decode($resp, true);
}

?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header text-center bg-primary text-white rounded">
					<h3>Crear Tipo de Habitacion</h3>
				</div>
				<div class="card-body">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/habitaciones/crearTipoDeHabitacion.php") ?> method="post">
						<div class="form-group pt-3">
							<input type="text" id="categoria" maxlength="15" minlength="2" name="categoria" class="form-control" placeholder="Categoria" required="required" autocomplete="off">
						</div>
						<div class="form-group pt-3">
							<input type="number" id="camas" maxlength="1" minlength="1" name="camas" class="form-control" placeholder="Camas" required="required" autocomplete="off">
						</div>
						<div class="form-group pt-3">
							<input type="number" id="precio" maxlength="7" minlength="5" name="precio" class="form-control" placeholder="Precio" required="required" autocomplete="off">
						</div>

						<div class="form-check pt-3">
							<input class="form-check-input" type="checkbox" value="true" id="terraza" name="terraza">
							<label class="form-check-label" for="flexCheckDefault">
								Terraza
							</label>
						</div>
						<div class="form-group text-center pt-3">
							<button type="submit" name="create_tipo" class="btn btn-outline-primary">Crear Tipo</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>