<?php
$tiposHabitacion = json_decode(file_get_contents('https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100'), true);
$tipo = $tiposHabitacion["habitacion"];

for ($i = 0; $i < count($tipo); $i++) {

	$lista_tiposHabitacion[$i]["uid"] = $tipo[$i]["uid"];
	$lista_tiposHabitacion[$i]["categoria"] = $tipo[$i]["categoria"];
}

?>

<?php

$url = "https://udmyhotelproject.herokuapp.com/myhotel/habitacion/";

if (isset($_POST["numero"]) && isset($_POST["tipo"])) {

	$numero = $_POST["numero"];
	$tipo = $_POST["tipo"];
	$token = $_SESSION["x-token"];

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$headers = array(
		"x-token: $token",
		"Content-Type: application/json",
	);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	$data = json_encode(array("numero" => (int)$numero, "tipo_habitacion" => $tipo), JSON_FORCE_OBJECT);

	var_dump($data);

	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$resp = curl_exec($curl);
	curl_close($curl);
	var_dump($resp);

	$data = json_decode($resp, true);
}else{

}

?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header text-center bg-primary text-white rounded">
					<h3>Crear Habitacion</h3>
				</div>
				<div class="card-body">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/habitaciones/crearHabitacion.php") ?> method="post">
						<div class="form-group pt-3">
							<input type="number" id="numero" maxlength="4" minlength="3" name="numero" class="form-control" placeholder="Numero de Habitación" required="required" autocomplete="off">
						</div>
						<div class="form-group pt-3">
							<select class="form-select pt-3" name="tipo" aria-label="Default select example">
								<option selected>Selecciona un tipo de Habitacion</option>
								<?php

								foreach ($lista_tiposHabitacion as $habitacionActual) {
							
									echo "<option value=". $habitacionActual["uid"] .">" . $habitacionActual["categoria"] . "</option>";
									
								}
								?>
								<option value="1">One</option>
								<option value="2">Two</option>
								<option value="3">Three</option>
							</select>
						</div>
						<div class="form-group text-center pt-3">
							<button type="submit" name="create_tipo" class="btn btn-outline-primary">Crear Habitacion</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>