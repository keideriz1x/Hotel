<?php
$servicio = json_decode(file_get_contents('https://udmyhotelproject.herokuapp.com/myhotel/servicio/?limite=100'), true);
$servicios = $servicio["servicio"];

for ($i = 0; $i < count($servicios); $i++) {

    $lista_servicios[$i]["nombre"] = $servicios[$i]["nombre"];
    $lista_servicios[$i]["descripcion"] = $servicios[$i]["descripcion"];
    $lista_servicios[$i]["precio"] = $servicios[$i]["precio"];
    $lista_servicios[$i]["img"] = $servicios[$i]["img"];
    $lista_servicios[$i]["estado"] = $servicios[$i]["estado"];
    $lista_servicios[$i]["uid"] = $servicios[$i]["uid"];
  
}

?>

<div class="container pb-1">
  <div class="row mt-4">
    <div class="col-lg-12">
      <div class="card text-white">
        <div class="card-header bg-primary text-center rounded">
          <h3>Consultar Servicio</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="table table-striped table-bordered" style="width:100%">
              <thead class="table-dark">
              <th scope="col"> Nombre</th>
              <th scope="col"> Descripcion</th>
                <th scope="col"> Precio</th>
                <th scope="col"> Servicios</th>
              </thead>
              <tbody>
                <?php

                foreach ($lista_servicios as $servicioActual) {
                  echo "<tr>";
                  echo "<td>" . $servicioActual["nombre"] . "</td>";
                  echo "<td>" . $servicioActual["descripcion"] . "</td>";
                  echo "<td>" . $servicioActual["precio"] . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $servicioActual["uid"] . "'><button class='btn btn-sm btn-outline-primary' id='cl" . $servicioActual["uid"] . "'>" . (($servicioActual["estado"] == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($servicioActual["estado"] == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.addEventListener("resize", mostrar);
    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    new $.fn.dataTable.FixedHeader(table);
  }
</script>