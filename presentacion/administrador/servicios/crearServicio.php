
<?php

$url = "https://udmyhotelproject.herokuapp.com/myhotel/servicio/";

if (isset($_POST["nombre"]) && isset($_POST["descripcion"]) && isset($_POST["precio"])) {

    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $precio = $_POST["precio"];

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $data = json_encode(array("nombre" => $nombre, "descripcion" => $descripcion, "precio" => $precio), JSON_FORCE_OBJECT);

    var_dump($data);

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);
    var_dump($resp);

    $data = json_decode($resp, true);
} else {
}

?>
<div class="container">
    <div class="row mt-4">
        <div class="col-3"></div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header text-center bg-primary text-white rounded">
                    <h3>Crear Servicio</h3>
                </div>
                <div class="card-body">
                    <form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/servicios/crearServicio.php") ?> method="post">
                        <div class="form-group pt-3">
                            <input type="text" id="nombre" maxlength="24" minlength="6" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
                        </div>
                        <div class="form-group pt-3">
                            <textarea class="form-control" placeholder="Descripcion" id="descripcion" name="descripcion" style="height: 100px"></textarea>
                        </div>
                        <div class="form-group pt-3">
                            <input type="number" id="precio" maxlength="6" minlength="5" name="precio" class="form-control" placeholder="Precio" required="required" autocomplete="off">
                        </div>
                        <div class="form-group text-center pt-3">
                            <button type="submit" name="create_tipo" class="btn btn-outline-primary">Crear Servicio</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>