<?php

if (isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["correo"]) && isset($_POST["cedula"])) {
	$nombre = $_POST["nombre"];
	$apellido = $_POST["apellido"];
	$correo = $_POST["correo"];
	$cedula = $_POST["cedula"];

	$url = "https://udmyhotelproject.herokuapp.com/myhotel/usuario";

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$headers = array(
		"Accept: application/json",
		"Content-Type: application/json",
	);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	$data = json_encode(array(
		"nombre" => strtolower($nombre), "apellido" => $apellido,
		"correo" => strtolower($correo), "clave" => $cedula,
		"rol" => "AUX"
	), JSON_FORCE_OBJECT);

	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$resp = curl_exec($curl);
	$data = json_decode($resp, true);

	curl_close($curl);
}

?>
<div class="container pb-1">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card ">
				<div class="card-header bg-primary text-center rounded text-white">
					<h3>Crear Auxiliar</h3>
				</div>
				<div class="card-body text-white">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/auxiliar/crearAuxiliar.php") ?> onsubmit="return validacion();" method="post">
						<div class="form-group pt-3">
							<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group pt-3">
							<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group pt-3">
							<input type="number" id="cedula" name="cedula" class="form-control" placeholder="Cedula" maxlength="30" minlength="8" size="10" required autocomplete="off">
						</div>
						<div class="form-group pt-3">
							<input type="email" id="correo" name="correo" class="form-control" placeholder="Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
						</div>
						<div class="form-group text-center pt-3">

							<button type="submit" name="create_cuenta" class="btn btn-outline-primary">Crear Auxiliar</button>
						</div>
					</form>
				</div>
			</div>
			<?php
			if (isset($data["msg"]) && $data["msg"] == "Ok") { ?>
				<div class="alert alert-success mt-3" role="alert">
					Cuenta creada exitosamente...
				</div>
			<?php
			}
			?>
			<?php
			if (isset($data["msg"]) && $data["msg"] == "error") { ?>
				<div class="alert alert-danger mt-3" role="alert">
					Este correo ya esta vinculado a una cuenta.
				</div>
			<?php
			}
			?>
		</div>
	</div>
</div>