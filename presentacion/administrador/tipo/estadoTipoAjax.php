<?php
if (isset($_GET["idTipo"]) && isset($_GET["estado"])) {
    $tipo = new Tipo($_GET["idTipo"], "", $_GET["estado"]);
    $tipo->editarEstado();

    $consultar = new Tipo($_GET["idTipo"], "", "");
    $consultar->consultar();

    $log = new Log("", "Editar estado de Tipo", $consultar->getNombre() . " cambiado a: " . (($_GET["estado"] == 1) ? "Activo" : "Restringido"), date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
    $log->crear();

    echo (($_GET["estado"] == 1) ? "<div id='icono" . $_GET["idTipo"] . "' class='text-light'>Activo</div>" : "<div id='icono" . $_GET["idTipo"] . "' class='text-light'>Restringido</div>");
}
