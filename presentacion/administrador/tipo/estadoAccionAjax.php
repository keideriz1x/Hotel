<?php
if (isset($_GET["idTipo"]) && isset($_GET["estado"])) {
	$idTipo = $_GET["idTipo"];
	$estado = $_GET["estado"];

	echo "<div id='accion" . $idTipo . "'><a href='index.php?pid= " . base64_encode("presentacion/administrador/tipo/editarTipo.php") . "&id=" . base64_encode($idTipo) . "' class ='btn btn-sm'><i class='fas fa-edit text-light' data-bs-toggle='tooltip' title='Editar'></i></a><button class='btn btn-sm' id='estado" . $idTipo . "' href='#' >" . (($estado == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($estado == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
}
?>
<?php
if (isset($_GET["idTipo"]) && isset($_GET["estado"])) { ?>
	<script>
		$(document).ready(function() {
			$("#estado<?php echo $idTipo ?>").click(function(e) {

				e.preventDefault();

				Swal.fire({
					title: '¿Seguro?',
					text: "¿Desea cambiar el estado de este tipo?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, hacer cambio',
					cancelButtonText: "Cancelar"
				}).then((result) => {
					if (result.isConfirmed) {

						$('[data-toggle="tooltip"]').tooltip('hide');

						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/tipo/estadoTipoAjax.php") ?>&idTipo=<?php echo $idTipo ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#icono<?php echo $idTipo ?>").load(url);
						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/tipo/estadoAccionAjax.php") ?>&idTipo=<?php echo $idTipo ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#accion<?php echo $idTipo ?>").load(url);

					}
				})

			});
		});
	</script>
<?php
}
?>