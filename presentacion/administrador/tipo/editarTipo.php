
<?php
$idTipo = null;
if (isset($_GET["id"])) {
    $idTipo = decrypt($_GET["id"], $key);
}

$editado = false;
if(isset($_POST["editarTipo"])){
    $tipo = new Tipo($idTipo, $_POST["nombre"], 1);
    $tipo -> editar();
    $editado = true;
}else{
    $tipo = new Tipo($idTipo);
    $tipo -> consultar();
}
?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Editar Tipo</h3>
				</div>
				<div class="card-body">
					<form id="editar" action=<?php echo "index.php?pid=" .base64_encode("presentacion/administrador/tipo/editarTipo.php"). "&id=" . base64_encode($idTipo) ?> method="post">
						<div class="form-group">
							<input id="nombre" maxlength="15" minlength="3" type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<button type="submit" name="editarTipo" class="btn btn-danger btn-block">Editar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if ($editado) { ?>
	<script>
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Tipo editado con exito.',
			showConfirmButton: false,
			timer: 1500
		})
	</script>
<?php } ?>