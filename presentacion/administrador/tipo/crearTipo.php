<?php

if (isset($_POST["create_tipo"])) {
	$t = new Tipo("", $_POST["nombre"], "");
	if ($t->verificar()) {
		$tipo = new Tipo("", $_POST["nombre"], 1);
		$tipo->crear();
		$log = new Log("", "Crear tipo", "Tipo: " . $_POST["nombre"], date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
		$log->crear();
		echo "<script>
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Tipo creado con exito.',
			showConfirmButton: false,
			timer: 1500
		})
		</script>";
	} else {
		echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Tipo ya existe.',
			})
		</script>";
	}
}
?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Tipo</h3>
				</div>
				<div class="card-body">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/tipo/crearTipo.php") ?> method="post">
						<div class="form-group">
							<input type="text" id="nombre" maxlength="15" minlength="2" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<button type="submit" name="create_tipo" class="btn btn-danger btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>