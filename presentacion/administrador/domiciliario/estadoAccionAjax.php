<?php

if (isset($_GET["idDomiciliario"]) && isset($_GET["estado"])) {

	$idDomiciliario = $_GET["idDomiciliario"];
	$estado = $_GET["estado"];

	echo "<div id='accion" . $idDomiciliario . "'><button class='btn btn-sm' id='dom" . $idDomiciliario . "'>" . (($estado == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($estado == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
}
?>
<?php
if (isset($_GET["idDomiciliario"]) && isset($_GET["estado"])) { ?>
	<script>
		$(document).ready(function() {
			$("#dom<?php echo $idDomiciliario ?>").click(function(e) {

				e.preventDefault();

				Swal.fire({
					title: '¿Seguro?',
					text: "¿Desea cambiar el estado de este domiciliario?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, hacer cambio',
					cancelButtonText: "Cancelar"
				}).then((result) => {
					if (result.isConfirmed) {

						$('[data-toggle="tooltip"]').tooltip('hide');

						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/domiciliario/estadoDomiciliarioAjax.php") ?>&idDomiciliario=<?php echo $idDomiciliario ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#icono<?php echo $idDomiciliario ?>").load(url);
						var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/domiciliario/estadoAccionAjax.php") ?>&idDomiciliario=<?php echo $idDomiciliario ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#accion<?php echo $idDomiciliario ?>").load(url);

					}
				})

			});
		});
	</script>
<?php
}
?>