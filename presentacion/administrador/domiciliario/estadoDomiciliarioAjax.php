<?php
if (isset($_GET["idDomiciliario"]) && isset($_GET["estado"])) {

    $domiciliario = new Domiciliario($_GET["idDomiciliario"], "", "", "", "", "", $_GET["estado"]);
    $domiciliario->editarEstado();

    $consultar = new Domiciliario($_GET["idDomiciliario"], "", "");
    $consultar->consultar();

    $log = new Log("", "Editar estado de Domiciliario", "id: ".$consultar->getIdDomiciliario()." nombre: ".$consultar->getNombre()." ".$consultar->getApellido() . " cambiado a: " . (($_GET["estado"] == 1) ? "Activo" : "Restringido"), date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
    $log->crear();

    echo (($_GET["estado"] == 1) ? "<div id='icono" . $_GET["idDomiciliario"] . "' class='text-light'>Activo</div>" : "<div id='icono" . $_GET["idDomiciliario"] . "' class='text-light'>Restringido</div>");
}
