<?php

if (isset($_POST["create_domiciliario"])) {
	$dom = new Domiciliario("", "", "", $_POST["correo"]);
	if ($dom->verificar()) {
		if (($_POST["correo"] == $_POST["cCorreo"]) && ($_POST["clave"] == $_POST["cClave"])) {

			$domiciliario = new Domiciliario("", $_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"], "", 1);
			$domiciliario->crear();

			$consultar= new Domiciliario("", "", "", $_POST["correo"], "", "", "");
			$consultar->consultarPorCorreo();

			$log = new Log("", "Crear domiciliario", "id: ". $consultar->getIdDomiciliario() . " Domiciliario: " . $_POST["nombre"] ." ".$_POST["apellido"] ." correo:". $_POST["correo"], date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
			$log->crear();

			echo "<script>
			Swal.fire({
				icon: 'success',
				title: 'Domiciliario ha sido creada con exito.',
				showConfirmButton: false,
				timer: 1500
			})
		</script>";
		} else {
			echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Contraseña o correo no coinciden.',
            })
		</script>";
		}
	} else {
		echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Este correo ya esta vinculado con una cuenta.',
			})
		</script>";
	}
}

?>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Domiciliario</h3>
				</div>
				<div class="card-body rounded">
					<form id="crear" action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/domiciliario/crearDomiciliario.php") ?> onsubmit="return validacion();" method="post">
						<div class="form-group">
							<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" required="required" autocomplete="off" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="cCorreo" name="cCorreo" placeholder="Confirmar Correo" required="required" autocomplete="off" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="clave" name="clave" placeholder="Clave" required="required" autocomplete="off" minlength="4" maxlength="15">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="cClave" name="cClave" placeholder="Confirmar Clave" required="required" autocomplete="off" minlength="4" maxlength="15">
						</div>
						<div class="form-group">
							<button type="submit" name="create_domiciliario" class="btn btn-danger btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
	$(function() {
		$(document).on('keyup', '#correo, #cCorreo', function() {
			var correo = $('#correo').val().trim();
			var cCorreo = $('#cCorreo').val().trim();
			if (!correo || !cCorreo || correo == '' || cCorreo == '') {
				$('#correo').removeClass('is-valid');
				$('#cCorreo').removeClass('is-valid');
			} else {
				if (correo !== cCorreo) {
					$('#correo').removeClass('is-valid').addClass('is-invalid');
					$('#cCorreo').removeClass('is-valid').addClass('is-invalid');
				} else {
					$('#correo').removeClass('is-invalid').addClass('is-valid');
					$('#cCorreo').removeClass('is-invalid').addClass('is-valid');
				}
			}
		});
	});
</script>
<script>
	$(function() {
		$(document).on('keyup', '#clave, #cClave', function() {
			var clave = $('#clave').val().trim();
			var cClave = $('#cClave').val().trim();
			if (!clave || !cClave || clave == '' || cClave == '') {
				$('#clave').removeClass('is-valid');
				$('#cClave').removeClass('is-valid');
			} else {
				if (clave !== cClave) {
					$('#clave').removeClass('is-valid').addClass('is-invalid');
					$('#cClave').removeClass('is-valid').addClass('is-invalid');
				} else {
					$('#clave').removeClass('is-invalid').addClass('is-valid');
					$('#cClave').removeClass('is-invalid').addClass('is-valid');
				}
			}
		});
	});
</script>
<script type="text/javascript">
    function validacion() {
        //obteniendo el valor que se puso en campo text del formulario
        var correo = $("#correo").val();
        var cCorreo = $("#cCorreo").val();
        var clave = $("#clave").val();
        var cClave = $("#cClave").val();
        //la condición
        if (((correo != cCorreo) || (clave != cClave)) || ((correo != cCorreo) && (clave != cClave))) {
            Swal.fire({
                icon: 'error',
                title: 'Contraseña o correo no coinciden.',
            });
            return false;
        }
        return true;
    }
</script>