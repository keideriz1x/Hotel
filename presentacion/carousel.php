<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css'>
<link rel="stylesheet" type="text/css" href="css/carousel.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js'></script>
<div id="sync1" class="owl-carousel owl-theme">
    <div class="item">
        <img src="https://www.hotelcapital.com.co/uploads/galeriahabitaciones/GHL-Capital-acomodacion-estandar.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://mediterrani.com/wp-content/uploads/2020/01/Tipos-de-habitaciones-de-un-hotel-2.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://st.depositphotos.com/1395424/2118/i/600/depositphotos_21182205-stock-photo-hotel-rooms.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://static.abc.es/Media/201504/27/hotel12--644x362.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2019/05/20152451/Mandarin-Oriental-Hong-Kong-3.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.estelarenaltoprado.com/cache/ed/0e/ed0e592be0b63955cd79f4ebb0c557ea.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.sonestacartagena.com/cache/68/0b/680b8ebb108c6c6f68369bd7c3ab99d8.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://hotellalaguna.com/wp-content/uploads/2018/01/habitaciones-hotel-la-laguna-pagina_0000s_0000_Junior-suite-hidromasaje-2.jpg" alt="">
    </div>
</div>

<div id="sync2" class="owl-carousel owl-theme">
    <div class="item">
    <img src="https://www.hotelcapital.com.co/uploads/galeriahabitaciones/GHL-Capital-acomodacion-estandar.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://mediterrani.com/wp-content/uploads/2020/01/Tipos-de-habitaciones-de-un-hotel-2.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://st.depositphotos.com/1395424/2118/i/600/depositphotos_21182205-stock-photo-hotel-rooms.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://static.abc.es/Media/201504/27/hotel12--644x362.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2019/05/20152451/Mandarin-Oriental-Hong-Kong-3.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.estelarenaltoprado.com/cache/ed/0e/ed0e592be0b63955cd79f4ebb0c557ea.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.sonestacartagena.com/cache/68/0b/680b8ebb108c6c6f68369bd7c3ab99d8.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://hotellalaguna.com/wp-content/uploads/2018/01/habitaciones-hotel-la-laguna-pagina_0000s_0000_Junior-suite-hidromasaje-2.jpg" alt="">
    </div>
</div>
<script>
    $(document).ready(function() {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");
        var slidesPerPage = 5;
        var syncedSecondary = true;

        sync1.owlCarousel({
            animateOut: 'fadeOutDown',
            animateIn: 'fadeInUp',
            items: 1,
            slideSpeed: 2000,
            nav: true,
            autoplay: true,
            dots: true,
            loop: true,
            responsiveRefreshRate: 200,
            navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        }).on('changed.owl.carousel', syncPosition);

        sync2
            .on('initialized.owl.carousel', function() {
                sync2.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                items: slidesPerPage,
                dots: false,
                nav: false,
                smartSpeed: 200,
                slideSpeed: 500,
                slideBy: slidesPerPage,
                responsiveRefreshRate: 100
            }).on('changed.owl.carousel', syncPosition2);

        function syncPosition(el) {

            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block

            sync2
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = sync2.find('.owl-item.active').length - 1;
            var start = sync2.find('.owl-item.active').first().index();
            var end = sync2.find('.owl-item.active').last().index();

            if (current > end) {
                sync2.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
                sync2.data('owl.carousel').to(current - onscreen, 100, true);
            }
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                sync1.data('owl.carousel').to(number, 100, true);
            }
        }

        sync2.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            sync1.data('owl.carousel').to(number, 300, true);
        });
    });
</script>