<?php
if (!isset($_SESSION["id"])) {
    include "presentacion/homeMenu.php";
}

$p = decrypt($_GET["p"], $key);

$producto = new Producto($p);
$producto->consultar();
?>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css'>
<link rel="stylesheet" type="text/css" href="css/carousel.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js'></script>

<div class="col-md-9 container pt-3">
    <div class="row">
        <div class="col-md-6">
            <div id="img1" class="owl-carousel owl-theme">

                <?php
                $foto = new Foto("", "", $p);
                $fotos = $foto->mostrarTodos();
                $contador = 0;
                $i = 1;
                if (!empty($fotos) and count($fotos) > 0) {
                    foreach ($fotos as $fotoActual) { ?>
                        <div class="item">
                            <img src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>">
                        </div>
                <?php           }
                }
                ?>

            </div>

            <div id="img2" class="owl-carousel owl-theme d-none d-sm-none d-md-block">

                <?php
                $foto = new Foto("", "", $p);
                $fotos = $foto->mostrarTodos();
                $contador = 0;
                $i = 1;
                if (!empty($fotos) and count($fotos) > 2) {
                    foreach ($fotos as $fotoActual) { ?>
                        <div class="item">
                            <img src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>">
                        </div>
                <?php           }
                }
                ?>

            </div>
        </div>
        <div class="col-md-6">
            <h5 class="text-light"><?php echo $producto->getNombre() ?></h5>
            <hr class="mt-4" style="background-color:#f80000">
            <div class="">
                <h5 class="pt-4 text-light">Precio: <b style="color: #f80000;">$ <?php echo number_format($producto->getPrecio(), 0, ',', '.') ?> COP</b></h5>
                <form action="#" method="post" enctype="multipart/form-data">
                    <div class="col-xl-5">
                        <div class="" style="min-width: 35px;max-width:65px">
                            <input type="number" id="numero" class="form-control mt-4" min="1" max="5" value="1" pattern="[0-5]">
                        </div>
                    </div>
                    <button type="submit" name="add-to-cart" class="btn btn-danger btn-lg mt-4 carritoP2">
                        Añadir al carrito</button>
                </form>
                <div class="row pt-5 text-light">
                    <div class="col-4 my-auto mx-auto d-none d-sm-none d-md-block">
                        <i aria-hidden="true" class="fas fa-lock"><span> Compra segura</span></i>
                    </div>

                    <div class="col-4 my-auto mx-auto d-none d-sm-none d-md-block">
                        <i aria-hidden="true" class="fas fa-truck"><span> Envíos ágiles</span></i>
                    </div>
                    <div class="col-4 my-auto mx-auto d-none d-sm-none d-md-block">
                        <i aria-hidden="true" class="fas fa-dollar-sign"><span> Precio garantizado</span> </i>
                    </div>
                </div>
            </div>
            <hr class="mt-4 d-none d-sm-none d-md-block" style="background-color:#f80000">
            <div class="pt-4 text-light d-none d-sm-none d-md-block">
                <span id="categoriaP"><b>Categoría: </b>
                    <?php
                    $productocategoria = new ProductoCategoria($p, "");
                    $productocategorias = $productocategoria->consultarTodos();
                    foreach ($productocategorias as $productocategoriaActual) {
                        $categoria = new Categoria($productocategoriaActual->getCategoria_idCategoria(), "", "");
                        $categorias = $categoria->mostrarTodos();
                        foreach ($categorias as $categoriaActual) {
                            echo $categoriaActual->getNombre();
                        }
                        if ($productocategoriaActual === end($productocategorias)) {
                            echo ".";
                        } else {
                            echo ", ";
                        }
                    }
                    ?>
                </span>
            </div>
            <div class="pt-4 text-light d-none d-sm-none d-md-block">
                <span id="tipoP"><b>Plataformas: </b>
                    <?php
                    $productotipo = new ProductoTipo($p, "");
                    $productotipos = $productotipo->consultarTodos();
                    foreach ($productotipos as $productotipoActual) {
                        $tipo = new Tipo($productotipoActual->getTipo_idTipo(), "", "");
                        $tipos = $tipo->mostrarTodos();
                        foreach ($tipos as $tipoActual) {
                            echo $tipoActual->getNombre();
                        }
                        if ($productotipoActual === end($productotipos)) {
                            echo ".";
                        } else {
                            echo ", ";
                        }
                    }
                    ?>
                </span>
            </div>
        </div>
    </div>

    <div class=" ">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                    <h5>Descripcion</h5>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                    <h5>Video</h5>
                </a>
            </li>
        </ul>
        <div class="tab-content text-light" id="myTabContent">
            <div class="tab-pane fade show active text-light" id="home" role="tabpanel" aria-labelledby="home-tab"><?php echo $producto->getDescripcion() ?></div>
            <div class="tab-pane fade video" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $producto->getVideo() ?>" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var input = document.getElementById('numero');
    input.addEventListener('input', function() {
        if (this.value.length > 1)
            this.value = this.value.slice(0, 1);
    })
</script>
<script>
    $(document).ready(function() {

        var img1 = $("#img1");
        var img2 = $("#img2");
        var slidesPerPage = 3;
        var syncedSecondary = true;

        img1.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: true,
            autoplay: true,
            dots: true,
            loop: true,
            responsiveRefreshRate: 200,
            navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        }).on('changed.owl.carousel', syncPosition);

        img2
            .on('initialized.owl.carousel', function() {
                img2.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                items: slidesPerPage,
                dots: false,
                nav: false,
                smartSpeed: 200,
                slideSpeed: 500,
                slideBy: slidesPerPage,
                responsiveRefreshRate: 100
            }).on('changed.owl.carousel', syncPosition2);

        function syncPosition(el) {

            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block

            img2
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = img2.find('.owl-item.active').length - 1;
            var start = img2.find('.owl-item.active').first().index();
            var end = img2.find('.owl-item.active').last().index();

            if (current > end) {
                img2.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
                img2.data('owl.carousel').to(current - onscreen, 100, true);
            }
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                img1.data('owl.carousel').to(number, 100, true);
            }
        }

        img2.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            img1.data('owl.carousel').to(number, 300, true);
        });
    });
</script>