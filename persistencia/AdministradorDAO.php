<?php
class AdministradorDAO{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    
    function AdministradorDAO ($pIdAdministrador, $pNombre, $pApellido, $pCorreo, $pClave,$pFoto) {
        $this -> idAdministrador = $pIdAdministrador;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> correo = $pCorreo;
        $this -> clave = $pClave;
        $this -> foto = $pFoto;
    }
    
    function autenticar () {        
        return "select idAdministrador 
                from administrador 
                where correo = '" . $this -> correo . "' and clave = sha1('" . $this -> clave . "')"; 
    }
    
    function consultar () {
        return "select nombre, apellido, correo,foto,clave
                from administrador
                where idAdministrador = '" . $this -> idAdministrador . "'";
    }

    public function editar(){
        return "update administrador
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . 
                "', correo = '" . $this -> correo . 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function editarClave(){
        return "update administrador
                set clave = '" .  sha1($this -> clave)  . 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }
    
    public function editarFoto(){
        return "update administrador
                set foto = '" .  $this -> foto  . 
                "' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    public function eliminarFoto(){
        return "update administrador
                set foto = '' where idAdministrador = '" . $this -> idAdministrador .  "'";
    }

    function consultarPorCorreo () {
        return "select idAdministrador, nombre, apellido
                from Administrador
                where correo = '" . $this -> correo . "'";
    }   
    
    function cambiarClave($id, $nuevaClave) {
        return "update Administrador set clave = sha1('" . $nuevaClave . "')
                where idAdministrador = '" . $id . "'";
    }
    
    
}

?>