<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $precio;
    private $cantidad;
    private $video;
    private $descripcion;
    
    function ProductoDAO ($pIdProducto, $pNombre, $pPrecio, $pCantidad, $pVideo, $pDescripcion) {
        $this -> idProducto = $pIdProducto;
        $this -> nombre = $pNombre;
        $this -> precio = $pPrecio;
        $this -> cantidad = $pCantidad;
        $this -> video = $pVideo;
        $this -> descripcion = $pDescripcion;
    }
    
    function consultar () {
        return "select idProducto, nombre, precio, cantidad, video, descripcion
                from producto
                where idProducto = '" . $this -> idProducto . "'";
    }

    function consultarPorNombre () {
        return "select idProducto
                from Producto
                where nombre = '" . $this -> nombre . "'";
    }
    
    function crear () {
        return "insert into producto (nombre,precio,cantidad,video,descripcion)
                values ('" . $this -> nombre . "', '" . $this -> precio . "', '" . $this -> cantidad . "', '" . $this -> video . "', '" . $this -> descripcion . "')";                
    }

    function verificar(){
        return "select * 
        from producto 
        where nombre='" . $this -> nombre . "'";
    }
    
    function consultarTodos () {
        return "select idProducto, nombre, precio, cantidad, video, descripcion
                from producto";
    }
    
    function editar () {
        return "update Producto 
                set nombre = '" . $this -> nombre . "'
                where idProducto = '" . $this -> idProducto . "'";
    }

    function editarEstado () {
        return "update Producto 
                set estado = '" . $this -> estado . "'
                where idProducto = '" . $this -> idProducto . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idProducto, nombre, estado
                from Producto
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idProducto, nombre, estado
                from Producto
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idProducto)
                from Producto";
    }

    function buscar($filtro){
        return "select idProducto, nombre, estado
                from Producto
                where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}
