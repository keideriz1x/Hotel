<?php
class ProductoTipoDAO{
    private $Producto_idProducto;
    private $Tipo_idTipo;
    
    function ProductoTipoDAO ($pProducto_idProducto, $pTipo_idTipo) {
        $this -> Producto_idProducto = $pProducto_idProducto;
        $this -> Tipo_idTipo = $pTipo_idTipo;
    }
    
    function consultar () {
        return "select Producto_idProducto, Tipo_idTipo
                from producto_has_tipo
                where idProductoTipo = '" . $this -> idProductoTipo . "'";
    }
    
    function crear () {
        return "insert into producto_has_tipo (Producto_idProducto,Tipo_idTipo)
                values ('" . $this -> Producto_idProducto . "', '" . $this -> Tipo_idTipo . "')";                
    }
    
    function consultarTodos () {
        return "select Producto_idProducto, Tipo_idTipo
                from producto_has_tipo
                where Producto_idProducto = '" . $this -> Producto_idProducto . "'";
    }
    
    function editar () {
        return "update producto_has_tipo 
                set Producto_idProducto = '" . $this -> Producto_idProducto . "'
                where idProductoTipo = '" . $this -> idProductoTipo . "'";
    }

    function editarTipo_idTipo () {
        return "update ProductoTipo 
                set Tipo_idTipo = '" . $this -> Tipo_idTipo . "'
                where idProductoTipo = '" . $this -> idProductoTipo . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idProductoTipo, Producto_idProducto, Tipo_idTipo
                from ProductoTipo
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idProductoTipo, Producto_idProducto, Tipo_idTipo
                from ProductoTipo
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idProductoTipo)
                from ProductoTipo";
    }

    function buscar($filtro){
        return "select idProductoTipo, Producto_idProducto, Tipo_idTipo
                from ProductoTipo
                where Producto_idProducto like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}

?>