<?php
class FotoDAO{
    private $idFoto;
    private $foto;
    private $Producto_idProducto;
    
    function FotoDAO ($pIdFoto, $pFoto, $pProducto_idProducto) {
        $this -> idFoto = $pIdFoto;
        $this -> foto = $pFoto;
        $this -> Producto_idProducto = $pProducto_idProducto;
    }
    
    function consultar () {
        return "select foto, Producto_idProducto
                from foto
                where idFoto = '" . $this -> idFoto . "'";
    }
    
    function crear () {
        return "insert into foto (foto,Producto_idProducto)
                values ('" . $this -> foto . "', '" . $this -> Producto_idProducto . "')";                
    }
    
    function consultarTodos () {
        return "select idFoto, foto, Producto_idProducto
                from foto";
    }

    function mostrarTodos () {
        return "select idFoto, foto, Producto_idProducto
                from foto
                where Producto_idProducto = '" . $this -> Producto_idProducto . "'";
    }
    
    function editar () {
        return "update Foto 
                set foto = '" . $this -> foto . "'
                where idFoto = '" . $this -> idFoto . "'";
    }

    function editarProducto_idProducto () {
        return "update Foto 
                set Producto_idProducto = '" . $this -> Producto_idProducto . "'
                where idFoto = '" . $this -> idFoto . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idFoto, foto, Producto_idProducto
                from Foto
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idFoto, foto, Producto_idProducto
                from Foto
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idFoto)
                from Foto";
    }

    function buscar($filtro){
        return "select idFoto, foto, Producto_idProducto
                from Foto
                where foto like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}

?>