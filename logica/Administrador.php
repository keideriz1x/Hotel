<?php
require "persistencia/AdministradorDAO.php";

class Administrador{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $conexion;
    private $administradorDAO;
    
    public function getIdAdministrador()
    {
        return $this->idAdministrador;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getFoto() {
		return $this->foto;
	}


    function Administrador ($pIdAdministrador="", $pNombre="", $pApellido="", $pCorreo="", $pClave="",$pFoto="") {
        $this -> idAdministrador = $pIdAdministrador;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> correo = $pCorreo;
        $this -> clave = $pClave;
        $this-> foto = $pFoto;
        $this -> conexion = new Conexion();
        $this -> administradorDAO = new AdministradorDAO($pIdAdministrador, $pNombre, $pApellido, $pCorreo, $pClave,$pFoto);        
    }
    
    function autenticar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $this -> idAdministrador = $this -> conexion -> extraer()[0];
            return true;
        }else{
            return false;
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto= $resultado[3];  
        $this -> clave = $resultado[4];              
    }

    function consultarPorCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarPorCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> apellido = $resultado[2];            
            return true;
        }else{
            return false;
        }
    }

    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> editar());
        $this -> conexion -> cerrar();
    }

    public function editarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> editarClave());
        $this -> conexion -> cerrar();
    }

    public function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }

    public function eliminarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> eliminarFoto());
        $this -> conexion -> cerrar();
    }
    
}


?>