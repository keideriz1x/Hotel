<?php
require "persistencia/InspectorDAO.php";

class Inspector{
    private $idInspector;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $InspectorDAO;
    
    public function getIdInspector()
    {
        return $this->idInspector;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getFoto()
    {
        return $this->foto;
    }
    
    function Inspector ($pIdInspector="", $pNombre="", $pApellido="", $pCorreo="", $pClave="", $pFoto="",$pEstado="" ) {
        $this -> idInspector = $pIdInspector;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> correo = $pCorreo;
        $this -> clave = $pClave;
        $this -> foto = $pFoto;
        $this -> estado = $pEstado;
        $this -> conexion = new Conexion();
        $this -> InspectorDAO = new InspectorDAO($pIdInspector, $pNombre, $pApellido, $pCorreo, $pClave, $pFoto, $pEstado);        
    }
    
    function autenticar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idInspector = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else{
            return false;
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> InspectorDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];    
        $this -> estado = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> clave = $resultado[5];
    }

    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> editar());
        $this -> conexion -> cerrar();
    }

    public function editarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> editarClave());
        $this -> conexion -> cerrar();
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> InspectorDAO -> crear());
        $this -> conexion -> cerrar();
    }

    function verificar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> verificar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 0){
            return true;
        }else{
            return false;
        }
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> InspectorDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Inspectors = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Inspectors, new Inspector($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]));
        }
        return $Inspectors;
    }
    
    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }
    
    function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }

    public function eliminarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> eliminarFoto());
        $this -> conexion -> cerrar();
    }
 
    function consultarPorCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> consultarPorCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idInspector = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> apellido = $resultado[2];
            $this -> correo = $resultado[3];
            return true;
        }else{
            return false;
        }
    }
    
    function cambiarClave($id, $nuevaClave){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> InspectorDAO -> cambiarClave($id, $nuevaClave));
        $this -> conexion -> cerrar();
    }
    
    
}


?>