<?php
require "persistencia/TipoDAO.php";

class Tipo{
    private $idTipo;
    private $nombre;
    private $estado;
    private $conexion;
    private $TipoDAO;
    
    /**
     * @return string
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    function Tipo ($pIdTipo="", $pNombre="", $pEstado="") {
        $this -> idTipo = $pIdTipo;
        $this -> nombre = $pNombre;
        $this -> estado = $pEstado;
        $this -> conexion = new Conexion();
        $this -> TipoDAO = new TipoDAO($pIdTipo, $pNombre, $pEstado);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> TipoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> estado = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> TipoDAO -> crear());
        $this -> conexion -> cerrar();
    }

    function verificar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> verificar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 0){
            return true;
        }else{
            return false;
        }
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Tipos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Tipos, new Tipo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Tipos;
    }

    function mostrarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> mostrarTodos());
        $this -> conexion -> cerrar();
        $Tipos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Tipos, new Tipo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Tipos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina, $orden, $dir){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> consultarPorPagina($cantidad, $pagina, $orden, $dir));
        $this -> conexion -> cerrar();
        $Tipoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Tipoes, new Tipo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Tipoes;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TipoDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> TipoDAO -> buscar($filtro));
        $this -> conexion -> cerrar();
        $Tipoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Tipoes, new Tipo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Tipoes;
    }
    
}


?>