<?php
require "persistencia/DomiciliarioDAO.php";

class Domiciliario{
    private $idDomiciliario;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $DomiciliarioDAO;
    
    public function getIdDomiciliario()
    {
        return $this->idDomiciliario;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getFoto()
    {
        return $this->foto;
    }
    
    function Domiciliario ($pIdDomiciliario="", $pNombre="", $pApellido="", $pCorreo="", $pClave="", $pFoto="",$pEstado="" ) {
        $this -> idDomiciliario = $pIdDomiciliario;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> correo = $pCorreo;
        $this -> clave = $pClave;
        $this -> foto = $pFoto;
        $this -> estado = $pEstado;
        $this -> conexion = new Conexion();
        $this -> DomiciliarioDAO = new DomiciliarioDAO($pIdDomiciliario, $pNombre, $pApellido, $pCorreo, $pClave, $pFoto, $pEstado);        
    }
    
    function autenticar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idDomiciliario = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else{
            return false;
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];    
        $this -> estado = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> clave = $resultado[5];              
    }

    function consultarPorNombre(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> consultarPorNombre());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> idDomiciliario = $resultado[0];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Domiciliarios = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Domiciliarios, new Domiciliario($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]));
        }
        return $Domiciliarios;
    }

    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function verificar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> verificar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 0){
            return true;
        }else{
            return false;
        }
    }

public function editarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> editarClave());
        $this -> conexion -> cerrar();
    }
    
    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }
    
    function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }

    public function eliminarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> eliminarFoto());
        $this -> conexion -> cerrar();
    }
 
    function consultarPorCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> consultarPorCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idDomiciliario = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> apellido = $resultado[2];
            $this -> correo = $resultado[3];
            return true;
        }else{
            return false;
        }
    }
    
    function cambiarClave($id, $nuevaClave){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> DomiciliarioDAO -> cambiarClave($id, $nuevaClave));
        $this -> conexion -> cerrar();
    }
    
    
}
