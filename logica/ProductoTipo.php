<?php
require "persistencia/ProductoTipoDAO.php";

class ProductoTipo{
    private $Producto_idProducto;
    private $Tipo_idTipo;
    private $conexion;
    private $ProductoTipoDAO;
    
    public function getProducto_idProducto()
    {
        return $this->Producto_idProducto;
    }

    public function getTipo_idTipo()
    {
        return $this->Tipo_idTipo;
    }


    function ProductoTipo ($pProducto_idProducto="", $pTipo_idTipo="") {
        $this -> Producto_idProducto = $pProducto_idProducto;
        $this -> Tipo_idTipo = $pTipo_idTipo;
        $this -> conexion = new Conexion();
        $this -> ProductoTipoDAO = new ProductoTipoDAO($pProducto_idProducto, $pTipo_idTipo);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> Producto_idProducto = $resultado[0];
        $this -> Tipo_idTipo = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $ProductoTipos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($ProductoTipos, new ProductoTipo($resultado[0], $resultado[1]));
        }
        return $ProductoTipos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoTipoDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
}


?>