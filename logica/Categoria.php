<?php
require "persistencia/CategoriaDAO.php";

class Categoria{
    private $idCategoria;
    private $nombre;
    private $estado;
    private $conexion;
    private $CategoriaDAO;
    

    
    /**
     * @return string
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    function Categoria ($pIdCategoria="", $pNombre="", $pEstado="") {
        $this -> idCategoria = $pIdCategoria;
        $this -> nombre = $pNombre;
        $this -> estado = $pEstado;
        $this -> conexion = new Conexion();
        $this -> CategoriaDAO = new CategoriaDAO($pIdCategoria, $pNombre, $pEstado);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> estado = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> crear());
        $this -> conexion -> cerrar();
    }

    function verificar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> verificar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 0){
            return true;
        }else{
            return false;
        }
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Categorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Categorias, new Categoria($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Categorias;
    }

    function mostrarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> mostrarTodos());
        $this -> conexion -> cerrar();
        $Categorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Categorias, new Categoria($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Categorias;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina, $orden, $dir){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultarPorPagina($cantidad, $pagina, $orden, $dir));
        $this -> conexion -> cerrar();
        $Categoriaes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Categoriaes, new Categoria($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Categoriaes;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> buscar($filtro));
        $this -> conexion -> cerrar();
        $Categoriaes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Categoriaes, new Categoria($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Categoriaes;
    }
    
}


?>