

<?php

session_start();

require "logica/Administrador.php";
require "logica/Cliente.php";
require "logica/Domiciliario.php";
require "logica/Inspector.php";
require "logica/Categoria.php";
require "logica/Log.php";
require "persistencia/Conexion.php";
require "logica/Tipo.php";

function getVisitorIp()
{
    // Recogemos la IP de la cabecera de la conexión
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ipAdress = $_SERVER['HTTP_CLIENT_IP'];
    }
    // Caso en que la IP llega a través de un Proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipAdress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Caso en que la IP lleva a través de la cabecera de conexión remota
    else {
        $ipAdress = $_SERVER['REMOTE_ADDR'];
    }
    return $ipAdress;
}

$user_agent = $_SERVER['HTTP_USER_AGENT'];

function getBrowser($user_agent)
{

    if (strpos($user_agent, 'MSIE') !== FALSE)
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
        return 'Microsoft Edge';
    elseif (strpos($user_agent, 'Trident') !== FALSE) //IE 11
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Opera Mini') !== FALSE)
        return "Opera Mini";
    elseif (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
        return "Opera";
    elseif (strpos($user_agent, 'Firefox') !== FALSE)
        return 'Mozilla Firefox';
    elseif (strpos($user_agent, 'Chrome') !== FALSE)
        return 'Google Chrome';
    elseif (strpos($user_agent, 'Safari') !== FALSE)
        return "Safari";
    else
        return 'No hemos podido detectar su navegador';
}

$navegador = getBrowser($user_agent);

$user_agent = $_SERVER['HTTP_USER_AGENT'];


function getPlatform($user_agent)
{
    $plataformas = array(
        'Windows 10' => 'Windows NT 10.0+',
        'Windows 8.1' => 'Windows NT 6.3+',
        'Windows 8' => 'Windows NT 6.2+',
        'Windows 7' => 'Windows NT 6.1+',
        'Windows Vista' => 'Windows NT 6.0+',
        'Windows XP' => 'Windows NT 5.1+',
        'Windows 2003' => 'Windows NT 5.2+',
        'Windows' => 'Windows otros',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
        'Mac otros' => 'Macintosh',
        'Android' => 'Android',
        'BlackBerry' => 'BlackBerry',
        'Linux' => 'Linux',
    );
    foreach ($plataformas as $plataforma => $pattern) {
        if (preg_match('/(?i)' . $pattern . '/', $user_agent))
            return $plataforma;
    }
    return 'Otras';
}

$SO = getPlatform($user_agent);

$key = "i";

function encrypt($string, $key)
{
    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }
    return base64_encode($result);
}

function decrypt($string, $key)
{
    $result = '';
    $string = base64_decode($string);
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}

$webPages = array(
   "presentacion/administrador/categoria/estadoAccionAjax.php",
   "presentacion/administrador/categoria/estadoCategoriaAjax.php",
   "presentacion/administrador/tipo/estadoAccionAjax.php",
   "presentacion/administrador/tipo/estadoTipoAjax.php",
   "presentacion/administrador/cliente/estadoAccionAjax.php",
   "presentacion/administrador/cliente/estadoClienteAjax.php",
   "presentacion/administrador/domiciliario/estadoAccionAjax.php",
   "presentacion/administrador/domiciliario/estadoDomiciliarioAjax.php",
   "presentacion/administrador/inspector/estadoInspectorAjax.php",
   "presentacion/administrador/inspector/estadoAccionAjax.php",
);

$pid = null;
if (isset($_GET["pid"])) {
    $pid = decrypt($_GET["pid"], $key);
}

if (isset($pid)) {
    if (isset($_SESSION["id"])) {
        if (in_array($pid, $webPages)) {
            include $pid;
        } else {
            header("Location: indexAjax.php");
        }
    }
}else {
    header("Location: indexAjax.php");
}

?>

