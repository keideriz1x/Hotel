<?php

session_start();

if (isset($_GET["sesion"]) && $_GET["sesion"] == 0) {
    unset($_SESSION["id"]);
    unset($_SESSION["rol"]);
    unset($_SESSION["nombre"]);
    unset($_SESSION["apellido"]);
    unset($_SESSION["img"]);
    unset($_SESSION["correo"] );
    unset($_SESSION["x-token"]);
    session_destroy();
    header("Location: index.php");
    exit();
}

$pid = null;
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}

$webPages = array(
    "presentacion/administrador/sesionAdministrador.php",
    "presentacion/cliente/sesionCliente.php",
    "presentacion/administrador/producto/crearProducto.php",
    "presentacion/administrador/producto/consultarProducto.php",
    "presentacion/home.php",
    "presentacion/administrador/categoria/crearCategoria.php",
    "presentacion/administrador/categoria/consultarCategoria.php",
    "presentacion/administrador/categoria/editarCategoria.php",
    "presentacion/administrador/tipo/consultarTipo.php",
    "presentacion/administrador/tipo/crearTipo.php",
    "presentacion/administrador/categoria/updateCategoria.php",
    "presentacion/administrador/tipo/editarTipo.php",
    "presentacion/administrador/cliente/consultarCliente.php",
    "presentacion/administrador/domiciliario/crearDomiciliario.php",
    "presentacion/domiciliario/sesionDomiciliario.php",
    "presentacion/administrador/domiciliario/consultarDomiciliario.php",
    "presentacion/administrador/inspector/crearInspector.php",
    "presentacion/inspector/sesionInspector.php",
    "presentacion/administrador/inspector/consultarInspector.php",
    "presentacion/cliente/editarPerfil.php",
    "presentacion/cliente/car.php",
    "presentacion/producto.php",
    //Menu Usuarios
    "presentacion/administrador/cliente/consultarCliente.php",
    "presentacion/administrador/auxiliar/consultarAuxiliar.php",
    "presentacion/administrador/auxiliar/crearAuxiliar.php",
    //Menu Habitaciones
    "presentacion/administrador/habitaciones/consultarHabitacion.php",
    "presentacion/administrador/habitaciones/consultarTipoDeHabitacion.php",
    "presentacion/administrador/habitaciones/crearHabitacion.php",
    "presentacion/administrador/habitaciones/crearTipoDeHabitacion.php",
    //Menu Servicios
    "presentacion/administrador/servicios/consultarServicio.php",
    "presentacion/administrador/servicios/crearServicio.php",

    //Menu auxiliar
    "presentacion/auxiliar/menuAuxiliar.php",
    "presentacion/auxiliar/sesionAuxiliar.php"
);

$pagSinSesion = array(
    "presentacion/olvidoClave.php",
    "presentacion/crearCuenta.php",
    "presentacion/cliente/car.php",
    "presentacion/autenticar.php",
    "presentacion/producto.php",
);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/5.1.1/morph/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js  "></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js "></script>

    <link rel="stylesheet" href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006288/BBBootstrap/choices.min.css?version=7.0.0">
    <script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006273/BBBootstrap/choices.min.js?version=7.0.0"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootpag/1.0.4/jquery.bootpag.min.js"></script>

    <script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.14/dist/jspdf.plugin.autotable.js"> </script>

    <script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>

    <title>My Hotel</title>
    <link rel="icon" type="image/png" href="img/icon.png">
</head>

<body class="contenedor bg-white">

    <?php
    if (isset($pid)) {
        echo $pid;
        if (isset($_SESSION["id"])) {
            if ($_SESSION["rol"] == "ADMIN") {
                include "presentacion/administrador/menuAdministrador.php";
            }
            if ($_SESSION["rol"] == "USER") {
                include "presentacion/homeMenu.php";
            }
            if ($_SESSION["rol"] == "AUX") {
                include "presentacion/auxiliar/menuAuxiliar.php";
            }
            if (in_array($pid, $webPages)) {
                include $pid;
            } else {
                include "presentacion/noExiste.php";
            }
        } else if (in_array($pid, $pagSinSesion)) {
            include $pid;
        } else {

            header("Location: index.php");
        }
    } else {
        if (isset($_SESSION["id"])) {
            include "presentacion/homeMenu.php";
        }
        include "presentacion/home.php";
    }

    if (isset($_SESSION["id"])) {
        if (($_SESSION["rol"] != "Administrador") && $_SESSION["rol"] != "Inspector") {
            include "presentacion/footer.php";
        }
    } else {
        include "presentacion/footer.php";
    }

    ?>

</body>

</html>